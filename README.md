# DuckDuckGo search results scraper

Collect all search results and save them to default dataset and key-value store. Actor run takes only a few seconds to collect all results - synchronous run is recommended.

**INPUT:**

| Field | Type | Optional | Description |
| ----- | ---- | ----------- | --- |
|SearchTerm|String|no|Define search term to look up by actor
|UrlParams|String|yes|Additional parameters to use in the search URL
|NumberOfResults|Integer|yes|Limit the number of returned and saved results
