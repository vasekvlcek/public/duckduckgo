const Apify = require('apify');

Apify.main(async () => {
	let delta = 0;
	const start = new Date(process.env.APIFY_STARTED_AT).getTime();
	const timer = setInterval(() => {
		delta = parseInt((Date.now() - start) / 1000);
		// console.log('DELTA', delta);
	}, 1000);

	const documentReady = () => document.readyState !== 'loading';
	const wait = ms => new Promise(ok => setTimeout(ok, ms));

	const input = await Apify.getValue('INPUT');
	if (typeof(input.SearchTerm) !== 'string') {
		throw new Error('Wrong input: !String');
	}
	console.log('Actor input:', input);

	const url = `https://duckduckgo.com/?q=${input.SearchTerm}${input.UrlParams || ''}`;
	const numberOfResults = parseInt(input.NumberOfResults) || 1000;
	console.log('Input to URL:', url);

	const browser = await Apify.launchPuppeteer();
	const page = await browser.newPage();

	await page.setRequestInterception(true);
	const resources = [
		'font',
		'image',
		'media',
		'stylesheet'
	];
	page.on('request', async request => {
		resources.includes(request.resourceType()) ?
			request.abort() :
			request.continue();
	});

	await page.goto(url);
	await page.waitForFunction(documentReady, {
		polling: 'mutation'
	});

	const loadMore = async (previousResultCount) => {
		let isOutOfResults = await page.evaluate(() => DDG.deep.isOutOfResults);
		let resultCount = await page.evaluate(() => DDG.deep.getResultCount());
		previousResultCount = previousResultCount || resultCount;

		console.log('LOAD MORE');
		await page.evaluate(() => {
			if (DDG.deep.hasPendingResults()) DDG.deep.showPendingResults();
			nsr(document.querySelector('a.result--more__btn.btn.btn--full'));
		});

		while (resultCount === previousResultCount && !isOutOfResults) {
			if (delta > 19) {
				// const screenBuffer = await page.screenshot({fullPage: true});
				// await Apify.setValue('latestScreen.png', screenBuffer, {contentType: 'image/png'});

                // throw new Error('Time out!', previousResultCount, resultCount);

                console.log('Timed out');
                const results = await page.evaluate(() => DDG.deep.results.d);
                return results.filter((input, index, array) => array.findIndex(item => item.u === input.u) === index);
			}
			await wait(100);
			resultCount = await page.evaluate(() => DDG.deep.getResultCount());
			isOutOfResults = await page.evaluate(() => DDG.deep.isOutOfResults);
		}
		if (isOutOfResults || numberOfResults < resultCount) {
			const results = await page.evaluate(() => DDG.deep.results.d);
			console.log(`${previousResultCount} -> ${results.length}`);
			return results;
		}
		console.log(`${previousResultCount} -> ${resultCount}`);
		return await loadMore(resultCount);
	};

	console.log('RUN QUERY');
	console.log('---------');
	const runTime = Date.now();
	const results = await loadMore();
	const endTime = Date.now();
	console.log('---------');
	console.log('END QUERY');

	console.log(`${results.length} results in ${parseFloat((endTime - runTime) / 1000).toFixed(2)} seconds`);

	const data = results
		.filter(result => result.u)
		.filter(result => result.t !== "EOF")
		.map(result => ({
			title: result.t,
			description: result.a,
			url: result.u
		}));

	const output = data.slice(0, numberOfResults);

	await Apify.pushData(output);
	await Apify.setValue('OUTPUT', output);
	console.log(`${output.length} results saved to output`);

	clearInterval(timer);
	console.log(`TOTAL RUNTIME: ${parseFloat((Date.now() - start) / 1000).toFixed(2)} seconds`);
});
